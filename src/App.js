import React from "react";
import { Switch, Route } from "react-router-dom";

import ProductList from "./pages/ProductList";
import ProductDetails from "./pages/ProductDetails";
import Layout from "./pages/Layout";

const App = () => {
  return (
    <Layout>
        <Switch>
          <Route path="/" exact component={ProductList} />
          <Route path="/product-details/:id" component={ProductDetails} />
        </Switch>
    </Layout>
  );
};
export default App;
