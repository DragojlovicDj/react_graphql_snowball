import React, { Fragment } from "react";
import { withStyles } from "@material-ui/core/styles";
import AppBar from "../components/AppBar";
import Container from "@material-ui/core/Container";

const style = theme => ({
    container : {
        paddingTop: theme.typography.pxToRem(80)
    }
});

const Layout = ({ children, classes }) => {
  return (
    <Fragment>
      <AppBar />
      <Container fixed className={classes.container}>
        {children}
      </Container>
    </Fragment>
  );
};
export default withStyles(style)(Layout);
