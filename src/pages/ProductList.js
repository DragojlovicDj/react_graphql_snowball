import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import isEmpty from "lodash/isEmpty";
import cleanDeep from "clean-deep";
import ProductCard from "../components/ProductCard";
import { Grid } from "@material-ui/core";
import CircularProgress from "@material-ui/core/CircularProgress";

const styles = theme => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    minWidth: 300,
    width: "100%"
  },
  image: {
    position: "relative",
    height: 200,
    [theme.breakpoints.down("xs")]: {
      width: "100% !important", // Overrides inline-style
      height: 100
    },
    "&:hover, &$focusVisible": {
      zIndex: 1,
      "& $imageBackdrop": {
        opacity: 0.15
      },
      "& $imageMarked": {
        opacity: 0
      },
      "& $imageTitle": {
        border: "4px solid currentColor"
      }
    }
  },
  focusVisible: {},
  imageButton: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    color: theme.palette.common.white
  },
  imageSrc: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundSize: "cover",
    backgroundPosition: "center 40%"
  },
  imageBackdrop: {
    position: "absolute",
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    backgroundColor: theme.palette.common.black,
    opacity: 0.4,
    transition: theme.transitions.create("opacity")
  },
  imageTitle: {
    position: "relative",
    padding: `${theme.spacing(2)}px ${theme.spacing(4)}px ${theme.spacing(1) +
      6}px`
  },
  imageMarked: {
    height: 3,
    width: 18,
    backgroundColor: theme.palette.common.white,
    position: "absolute",
    bottom: -2,
    left: "calc(50% - 9px)",
    transition: theme.transitions.create("opacity")
  },
  spiner: {
    paddingTop: 300
  }
});

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      data: []
    };
  }

  componentDidMount() {
    this.fetchProducts();
  }

  getData(data) {
    const cleanObject = cleanDeep(data, { emptyStrings: false }); // cleans empy objects and array
    console.log(cleanObject);
    return cleanObject;
  }

  fetchProducts = () => {
    const query = `
    {
        tree(language: "en", path: "/") {
          ... on Item {
            children {
              ... on Product {
                id
                name
                path
                variants {
                  name
                  sku
                  price
                  stock
                  isDefault
                  image {
                    url
                    altText
                    variants {
                      url
                      width
                    }
                  }
                }
              }
            }
          }
        
      }
    }`;

    this.setState({ loading: true }, () => {
      fetch("https://graph.crystallize.com/tenant/demo", {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify({ query: query })
      })
        .then(r => r.json())
        .then(r => {
          const validData = this.getData(r.data.tree);
          this.setState({ loading: false, data: validData });
        });
    });
  };
  render() {
    const { classes } = this.props;
    const { data } = this.state;
    return (
      <Grid container justify="space-evenly" spacing={10}>
        {isEmpty(data) ? (
          <div className={classes.spiner}>
            <CircularProgress size={80} />
          </div>
        ) : (
          data.map((products, index) => {
            return (
              <Grid item key={`product-${index}`} xs={3}>
                <ProductCard products={products.children} />
              </Grid>
            );
          })
        )}
      </Grid>
    );
  }
}

export default withStyles(styles)(ProductList);
