import React, { Component } from "react";
import ProductDetailsList from "../components/ProductDetailsList";
import ProductDetailsImage from "../components/ProductDetailsImage";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";

class ProductDetails extends Component {
  render() {
    return (
      <Paper elevation={12} style={{ padding: 10, marginBottom: 20}}>
        <Grid container justify="space-between">
          <Grid item xs={6}>
            <ProductDetailsImage
              product={JSON.parse(localStorage.getItem("product"))}
            />
          </Grid>
          <Grid item xs={6}>
            <Grid container justify="center">
              <Grid item>
                <ProductDetailsList
                  product={JSON.parse(localStorage.getItem("product"))}
                />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Paper>
    );
  }
}

export default ProductDetails;
