import React from "react";
import { defaultImg } from "../assets/image";

const ProductDetailsImage = props => {
  const { product } = props;
  return (
    <div style={{ maxWidth: 600, height: "100%" }}>
      <img
        src={
          product.variants[0].image
            ? product.variants[0].image.url
            : defaultImg
        }
        alt="Contemplative Reptile"
        style={{ objectFit: "contain", width: "100%", height: "100%" }}
      />
    </div>
  );
};

export default ProductDetailsImage;
