import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import LocalOffer from "@material-ui/icons/LocalOffer";
import Layers from "@material-ui/icons/Layers";
import Subtitles from "@material-ui/icons/Subtitles";
import MonetizationOn from "@material-ui/icons/MonetizationOn";


const styles = theme => ({
  root: {
    textAlign: "center",
    flex: "0 0 auto",
    fontSize: theme.typography.pxToRem(24),
    padding: 12,
    borderRadius: "50%",
    color: theme.palette.action.active,
    transition: theme.transitions.create("background-color", {
      duration: theme.transitions.duration.shortest
    }),
    "&:hover": {
      backgroundColor: "transparent",
      cursor: "default"
    },
    "&$disabled": {
      backgroundColor: "transparent",
      color: theme.palette.action.disabled,
      cursor: "default"
    }
  },
  demo: {
    backgroundColor: theme.palette.background.paper
  },
  title: {
    margin: theme.spacing(2, 0, 2)
  },
  icon: {
    textAlign: "center",
    paddingTop: 15
  },
  ProductDetailsList: {
    margin: 10
  },
  container: {
    padding: theme.spacing(2, 4)
  }
});

const ProductDetailsList = props => {
  const { classes, product } = props;

  return (
    <Grid container justify="flex-start" className={classes.container}>
      <Grid item xs={12}>
        <Typography variant="h4" className={classes.title}>
          Product details:
        </Typography>
      </Grid>
      <Grid item xs={3} className={classes.icon}>
        <LocalOffer />
      </Grid>
      <Grid item xs={9}>
        <Typography variant="h6" className={classes.title}>
          <b>Name:</b> {product.name}
        </Typography>
      </Grid>
      <Grid item xs={3} className={classes.icon}>
        <Subtitles />
      </Grid>
      <Grid item xs={9}>
        <Typography variant="h6" className={classes.title}>
        <b>Sku:</b> {product.variants[0].sku}
        </Typography>
      </Grid>
      <Grid item xs={3} className={classes.icon}>
        <Layers />
      </Grid>
      <Grid item xs={9}>
        <Typography variant="h6" className={classes.title}>
        <b>Stock:</b> {product.variants[0].stock}
        </Typography>
      </Grid>
      <Grid item xs={3} className={classes.icon}>
        <MonetizationOn />
      </Grid>
      <Grid item xs={9}>
        <Typography variant="h6" className={classes.title}>
        <b>Price:</b> {product.variants[0].price}$
        </Typography>
      </Grid>
    </Grid>
  );
};

export default withStyles(styles)(ProductDetailsList);
