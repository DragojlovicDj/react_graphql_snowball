import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import { Link } from "react-router-dom";
import clsx from "clsx";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import CardActions from "@material-ui/core/CardActions";
import Collapse from "@material-ui/core/Collapse";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { red } from "@material-ui/core/colors";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import ProductChip from "./ProductChip";
import { defaultImg } from "../assets/image";

const styles = theme => ({
  card: {
    minWidth: 182,
    marginTop: 20
  },
  media: {
    height: 0,
    paddingTop: "56.25%" // 16:9
  },
  expand: {
    transform: "rotate(0deg)",
    marginLeft: "auto",
    transition: theme.transitions.create("transform", {
      duration: theme.transitions.duration.shortest
    })
  },
  expandOpen: {
    transform: "rotate(180deg)"
  },
  avatar: {
    backgroundColor: red[500]
  },
  cardHeader: {
    textTransform: "capitalize"
  },
  chipContainer: {
    display: "flex",
    justifyContent: "left",
    flexWrap: "wrap"
  },
  paragraph: {
    marginLeft: 20
  }

});

const defaultProduct = {
  path: "/",
  variants: [{ image: { url: defaultImg } }]
};

class ProductCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };

    this.handleExpandClick = this.handleExpandClick.bind(this);
  }

  handleExpandClick() {
    const { expanded } = this.state;
    this.setExpanded(!expanded);
  }

  setExpanded(expanded) {
    this.setState({ expanded });
  }

  showDetailsData = product => {
    localStorage.setItem("product", JSON.stringify(product));
  };

  render() {
    const { classes, products } = this.props;
    const { expanded } = this.state;
    const {
      path = "/",
      variants: [{ image: { url: img = defaultImg } = { img: defaultImg } }]
    } = products[0] || defaultProduct; //object destructoring on props

    const productName = path.split("/")[1].toLowerCase();

    return (
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar aria-label="Recipe" className={classes.avatar}>
              {productName.charAt(0)}
            </Avatar>
          }
          className={classes.cardHeader}
          title={productName}
        />
        <CardMedia className={classes.media} image={img} title="Paella dish" />
        <CardContent>
          <Typography variant="body2" color="textSecondary" component="p" />
        </CardContent>
        <CardActions disableSpacing>
        <p className={classes.paragraph}>Show More</p>
          <IconButton
            className={clsx(classes.expand, {
              [classes.expandOpen]: expanded
            })}
            onClick={this.handleExpandClick}
            aria-expanded={expanded}
            aria-label="Show more"
          >
            <ExpandMoreIcon />
          </IconButton>
        </CardActions>
        <Collapse in={expanded} timeout="auto" unmountOnExit>
          <CardContent>
            {products.map(product => {
              return (
                <Link
                  style={{ textDecoration: 'none' }}
                  to={"/product-details/" + product.id}
                  key={product.id}
                  onClick={() => this.showDetailsData(product)}
                >
                  <div className={classes.chipContainer}>
                    <ProductChip
                      image={
                        product.variants[0].image
                          ? product.variants[0].image.url
                          : defaultImg
                      }
                      text={product.name}
                    />
                  </div>
                </Link>
              );
            })}
          </CardContent>
        </Collapse>
      </Card>
    );
  }
}

export default withStyles(styles)(ProductCard);
