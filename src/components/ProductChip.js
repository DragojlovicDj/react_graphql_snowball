import React from "react";
import { withStyles } from "@material-ui/core/styles";
import Avatar from "@material-ui/core/Avatar";
import Chip from "@material-ui/core/Chip";

const styles = theme => ({
  chip: {
    marginBottom: theme.typography.pxToRem(5)
  }
});

const ProductChip = props => {
  const { image, text, onChipClick, classes } = props;
  return (
    <Chip
      avatar={<Avatar src={image} />}
      label={text}
      onClick={onChipClick}
      className={classes.chip}
      variant="outlined"
    />
  );
};

export default withStyles(styles)(ProductChip);
