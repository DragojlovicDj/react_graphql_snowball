import React, { Fragment } from "react";

import AppBarMUI from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { withRouter } from "react-router-dom";
import { withStyles } from "@material-ui/core/styles";

import ArrowBackIos from "@material-ui/icons/ArrowBackIos";

const styles = theme => ({
  color: {
    color: "white"
  }
});

const HOME_PAGE = "/";

const AppBar = props => {
  const { classes, history } = props;

  const handleExpandClick = props => {
    history.push("/");
  };

  const { pathname: path } = props.location;
  return (
    <AppBarMUI color="primary" position="static">
      <Toolbar>
        {path !== HOME_PAGE && (
            <IconButton onClick={handleExpandClick} aria-label="Go back">
              <ArrowBackIos className={classes.color} />
            <Typography variant="h6" className={classes.color}>
              BACK
            </Typography>
            </IconButton>
        )}
        <div style={{ flexGrow: 1 }} />
        <Typography variant="h6" color="inherit">
          Product App
        </Typography>
      </Toolbar>
    </AppBarMUI>
  );
};

export default withRouter(withStyles(styles)(AppBar));
